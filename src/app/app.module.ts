import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import { OrderModule } from 'ngx-order-pipe';

import {AppComponent} from './app.component';
import {MountsComponent} from "./components/mounts/mounts.component";
import {WoWService} from "./services/WoW.service";
import {HttpClientModule} from "@angular/common/http";
import {HomeComponent} from "./components/home/home.component";
import {MountDetailComponent} from "./components/mountDetail/mountDetail.component";
import {MainComponent} from "./components/main/main.component";
import {NgxPaginationModule} from "ngx-pagination";
import {LootedPipe} from "./pipes/looted.pipe";
import {CharacterComponent} from "./components/character/character.component";
import {ReactiveFormsModule} from "@angular/forms";
import {StorageServiceModule} from "angular-webstorage-service";

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        MainComponent,
        MountsComponent,
        MountDetailComponent,
        CharacterComponent,

        LootedPipe
    ],
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        OrderModule,
        NgxPaginationModule,
        HttpClientModule,
        StorageServiceModule
    ],
    providers: [WoWService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
