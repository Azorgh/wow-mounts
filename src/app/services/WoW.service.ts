import {Inject, Injectable} from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import {SESSION_STORAGE, WebStorageService} from "angular-webstorage-service";

@Injectable()
export class WoWService{
    // base_url:string = "http://127.0.0.1:8000/";
    base_url:string = "http://wow.test/";
    constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService, private http:HttpClient){}

    getMounts(){
        let character = this.storage.get('character');
        let lang = navigator.language || "en";
        let character = this.storage.get('character');
        return this.http.get(this.base_url + 'mounts?realm='+ character.realm +'&name=' + character.name + "&lang=" + lang + "&region=" + character.region);
    }

    getRealms(region){
        return this.http.get(this.base_url + 'realms?region=' + region);
    }

    getCharacter(values){
        return this.http.post(this.base_url + 'character', values);
    }
}