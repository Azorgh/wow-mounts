import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: "looted"})
export class LootedPipe implements PipeTransform{
    transform(mounts:any[], looted: boolean){
        if(looted == null)
            return mounts;

        return mounts.filter(mount => mount.looted == looted);
    }
}