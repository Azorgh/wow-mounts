import {Component, EventEmitter, HostBinding, Inject, Input, Output} from '@angular/core';
import {WoWService} from "../../services/WoW.service";
import {OrderPipe} from "ngx-order-pipe";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SESSION_STORAGE, WebStorageService} from "angular-webstorage-service";

@Component({
    selector: '[appCharacters]',
    templateUrl: './character.component.html'
})
export class CharacterComponent{
    @HostBinding('class') cueClass:string='menu';
    characterForm: FormGroup;
    currentCharacter:any = null;
    submitted = false;
    error:any = null;

    @Output() characterLoaded = new EventEmitter();

    realms:any = [];
    constructor(@Inject(SESSION_STORAGE) private storage: WebStorageService,
                public wow: WoWService,
                private orderPipe: OrderPipe,
                private formBuilder: FormBuilder
    ){}

    ngOnInit(){
        /**
         * On init, if local storage got character property,
         * use this data to get default hero
         */
        let oldCharacter = this.storage.get('character');

        this.characterForm = this.formBuilder.group({
            region: [oldCharacter ? oldCharacter.region : 'EU', Validators.required],
            realm: [oldCharacter ? oldCharacter.realm : 'kirin-tor', Validators.required],
            name: [oldCharacter ? oldCharacter.name : '', Validators.required],
        });


        this.wow.getRealms(this.characterForm.value.region).subscribe((data:any) => {
            this.realms = this.orderPipe.transform(data, 'name', false, true);
        });

        if(oldCharacter){
            this.onSubmit();
        }



        this.characterLoaded.emit(false);
    }

    regionChange($event){
        this.characterForm.controls['realm'].patchValue("");
        this.wow.getRealms(this.characterForm.value.region).subscribe((data:any) => {
            this.realms = this.orderPipe.transform(data, 'name', false, true);
        });
    }

    getWowIconImg(avatar){
        let src = `http://render-eu.worldofwarcraft.com/character/${avatar}`;
        return { backgroundImage: `url(${src})` }
    }

    onSubmit() {
        this.characterLoaded.emit(false);
        this.currentCharacter = null;
        if (this.characterForm.invalid) {

        }else{
            this.submitted = true;
            this.storage.set("character", this.characterForm.value);
            this.wow.getCharacter(this.characterForm.value).subscribe((data:any) => {
                this.error = null;
                this.currentCharacter = data;
                this.characterLoaded.emit(this.currentCharacter);

                if(this.currentCharacter.faction == 0){
                    this.cueClass = "menu blue";
                }else{
                    this.cueClass = "menu";
                }
            }, error => {
                this.error = error.error.message;
            });
        }
    }
}