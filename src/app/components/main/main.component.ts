import {Component, EventEmitter, HostBinding, Input, OnInit, Output} from "@angular/core";

@Component({
    selector: '[appMain]',
    templateUrl: './main.component.html',
})
export class MainComponent{
    @HostBinding('class') mainClass:string ='main';
    @Output() mountSelected = new EventEmitter();
    @Input() character:any;
    // mountSelected = null;

    mountIsSelected($event){
        this.mountSelected.emit($event);
    }
}