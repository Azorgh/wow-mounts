import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {WoWService} from "../../services/WoW.service";
import { OrderPipe } from 'ngx-order-pipe';
import {LootedPipe} from "../../pipes/looted.pipe";

@Component({
    selector: '[appMounts]',
    templateUrl: './mounts.component.html',
    styleUrls: ['./mounts.component.css'],
    providers: [LootedPipe]
})
export class MountsComponent{
    mounts:any = [];
    title: string = "";
    mountsDisplayed:any = [];
    mountsCount:number = 0;
    loading:boolean = true;
    filters:any = [];
    filterId:number = 1;

    @Input() character = null;
    @Output() mountSelected = new EventEmitter();

    constructor(public wow: WoWService, private orderPipe: OrderPipe, private lootedPipe: LootedPipe){}

    ngOnInit(){
        this.filters = [
            {id: 1, name: "All"},
            {id: 2, name: "Collected"},
            {id: 3, name: "Missing"},
            {id: 4, name: "Favorites"},
        ];

        this.getFilter(1);
    }

    ngOnChanges(changes){
        if(changes.character){
            this.loading = true;
            if(this.character){
                this.wow.getMounts().subscribe((data:any) => {
                    this.mounts = this.orderPipe.transform(data, 'name', false, true);
                    this.mountsDisplayed = this.mounts;
                    this.mountsCount = data.length;
                    this.loading = false;
                });
            }else{
                this.mounts = [];
                this.mountsDisplayed = [];
                this.mountsCount = 0;
                this.loading = false;
            }

            this.getFilter(1);
        }
    }

    getWowIconImg(icon){
        let src = `https://wow.zamimg.com/images/wow/icons/large/${icon}.jpg`;
        return { backgroundImage: `url(${src})` }
    }

    selectMount(mount){
        this.mountSelected.emit(mount);
    }

    getFilter(id){
        this.filterId = id;

        if(id == 1){
            this.mountsDisplayed = this.lootedPipe.transform(this.mounts, null);
            this.mountsCount = this.mountsDisplayed.length;
            this.title = "All mounts";
        }else if(id == 2){
            this.mountsDisplayed = this.lootedPipe.transform(this.mounts, true);
            this.mountsCount = this.mountsDisplayed.length;
            this.title = "Collected mounts";
        }else if(id == 3){
            this.mountsDisplayed = this.lootedPipe.transform(this.mounts, false);
            this.mountsCount = this.mountsDisplayed.length;
            this.title = "Missing mounts";
        }
    }
}