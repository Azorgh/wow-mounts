import {Component, HostBinding, Input} from '@angular/core';

@Component({
    selector: '[appMountDetail]',
    templateUrl: './mountDetail.component.html'
})
export class MountDetailComponent{
    @HostBinding('class') cueClass:string='cue';
    @Input() mount:any = null;
    @Input() characterLoaded:any = null;

    ngOnChanges(changes){
        if(changes.characterLoaded){
            if(this.characterLoaded && this.characterLoaded.faction == 0){
                this.cueClass = "cue blue";
            }else{
                this.cueClass = "cue";
            }
        }
    }

    getWowIconImg(icon){
        let src = `https://wow.zamimg.com/images/wow/icons/large/${icon}.jpg`;
        return { backgroundImage: `url(${src})` }
    }
}