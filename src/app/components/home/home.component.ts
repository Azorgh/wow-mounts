import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: '[appHome]',
    templateUrl:'./home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent {
    mountSelected:any = [];
    characterSelected:any = null;

    mountIsSelected($event){
        this.mountSelected = $event;
    }

    characterIsLoaded($event){
        this.characterSelected = $event;
    }
}